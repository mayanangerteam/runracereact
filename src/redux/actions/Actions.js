import ActionKeys from "../../keys/ActionKeys";
import ActionStatusKeys from "../../keys/ActionStatusKeys";

// Register

export function startRegisterAction() {
    return {
        type: ActionKeys.REGISTER_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorRegisterAction(error) {
    return {
        type: ActionKeys.REGISTER_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successRegisterAction() {
    return {
        type: ActionKeys.REGISTER_ACTION,
        status: ActionStatusKeys.SUCCESS,
    }
}

// Login

export function startLoginAction() {
    return {
        type: ActionKeys.LOGIN_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorLoginAction(error) {
    return {
        type: ActionKeys.LOGIN_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successLoginAction(refreshKey, accessKey) {
    return {
        type: ActionKeys.LOGIN_ACTION,
        status: ActionStatusKeys.SUCCESS,
        refreshKey: refreshKey,
        accessKey: accessKey,
    }
}

// Ask Recover password

export function startAskRecoverAction() {
    return {
        type: ActionKeys.ASK_RECOVER_PASSWORD_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorAskRecoverAction(error) {
    return {
        type: ActionKeys.ASK_RECOVER_PASSWORD_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successAskRecoverAction() {
    return {
        type: ActionKeys.ASK_RECOVER_PASSWORD_ACTION,
        status: ActionStatusKeys.SUCCESS,
    }
}

// Approve recover password

export function startApproveRecoverAction() {
    return {
        type: ActionKeys.APPROVE_RECOVER_PASSWORD_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorApproveRecoverAction(error) {
    return {
        type: ActionKeys.APPROVE_RECOVER_PASSWORD_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successApproveRecoverAction() {
    return {
        type: ActionKeys.APPROVE_RECOVER_PASSWORD_ACTION,
        status: ActionStatusKeys.SUCCESS,
    }
}

// Profile

export function startProfileAction() {
    return {
        type: ActionKeys.PROFILE_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorProfileAction(error) {
    return {
        type: ActionKeys.PROFILE_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successProfileAction(account) {
    return {
        type: ActionKeys.PROFILE_ACTION,
        status: ActionStatusKeys.SUCCESS,
        account: account,
    }
}

// Add horse

export function startAddHorseAction() {
    return {
        type: ActionKeys.ADD_HORSE_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorAddHorseAction(error) {
    return {
        type: ActionKeys.ADD_HORSE_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successAddHorseAction(horse) {
    return {
        type: ActionKeys.ADD_HORSE_ACTION,
        status: ActionStatusKeys.SUCCESS,
        horse: horse
    }
}

// Get horses

export function startGetHorsesAction() {
    return {
        type: ActionKeys.GET_HORSES_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorGetHorsesAction(error) {
    return {
        type: ActionKeys.GET_HORSES_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successGetHorsesAction(horses) {
    return {
        type: ActionKeys.GET_HORSES_ACTION,
        status: ActionStatusKeys.SUCCESS,
        horses: horses,
    }
}

// Start race

export function startStartRaceAction() {
    return {
        type: ActionKeys.START_RACE_ACTION,
        status: ActionStatusKeys.START,
    }
}

export function errorStartRaceAction(error) {
    return {
        type: ActionKeys.START_RACE_ACTION,
        status: ActionStatusKeys.ERROR,
        error: error,
    }
}

export function successStartRaceAction(race) {
    return {
        type: ActionKeys.START_RACE_ACTION,
        status: ActionStatusKeys.SUCCESS,
        race: race,
    }
}

// Logout

export function logoutAction() {
    return {
        type: ActionKeys.LOGOUT
    }
}
