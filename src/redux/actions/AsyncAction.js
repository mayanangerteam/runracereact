
import {
    errorAddHorseAction,
    errorApproveRecoverAction,
    errorAskRecoverAction, errorGetHorsesAction,
    errorLoginAction, errorProfileAction, errorRegisterAction, errorStartRaceAction, startAddHorseAction,
    startApproveRecoverAction,
    startAskRecoverAction, startGetHorsesAction,
    startLoginAction,
    startProfileAction,
    startRegisterAction, startStartRaceAction, successAddHorseAction, successApproveRecoverAction,
    successAskRecoverAction, successGetHorsesAction,
    successLoginAction, successProfileAction,
    successRegisterAction, successStartRaceAction
} from "./Actions";
import ConnectionService from "../../services/ConnectionService";
import Urls from "../../model/Urls";
import {findError} from "../../services/ToolsService";

export function registerAsyncAction(email, password) {
    return (dispatch, getState) => {

        console.log("hello");

        dispatch(startRegisterAction());

        ConnectionService.standard(
            Urls.REGISTER_PATH,
            {account: {email: email, password: password}},
            (res) => {
                const errorCode = findError(res);
                console.log({res, errorCode});

                if (errorCode) {
                    dispatch(errorRegisterAction(errorCode));
                    return;
                }
                dispatch(successRegisterAction());
            }
        )
    }
}

export function loginAsyncAction(email, password) {
    return (dispatch, getState) => {

        dispatch(startLoginAction());

        ConnectionService.auth(
            email,
            password,
            res => {
                const errorCode = findError(res);
                if (errorCode) {
                    dispatch(errorLoginAction(errorCode));
                    return;
                }
                dispatch(successLoginAction(res.response.refresh_token, res.response.access_token));
            }
        )

    }
}

export function askRecoverAsyncAction(email) {
    return (dispatch, getState) => {

        dispatch(startAskRecoverAction());

        ConnectionService.standard(
            Urls.ASK_RECOVER_PASSWORD_PATH,
            {email: email},
            res => {
                const errorCode = findError(res);

                if (errorCode) {
                    dispatch(errorAskRecoverAction(errorCode));
                    return;
                }
                dispatch(successAskRecoverAction());
            }
        )
    }
}

export function approveRecoverAsyncAction(email, secretCode, newPassword) {
    return (dispatch, getState) => {

        dispatch(startApproveRecoverAction());

        ConnectionService.standard(
            Urls.APPROVE_RECOVER_PASSWORD_PATH,
            {email: email, secretCode: secretCode, newPassword: newPassword},
            res => {
                const errorCode = findError(res);

                if (errorCode) {
                    dispatch(errorApproveRecoverAction(errorCode));
                    return;
                }
                dispatch(successApproveRecoverAction());
            }
        )
    }
}

export function profileAsyncAction() {
    return (dispatch, getState) => {
        const refreshKey = getState().data.refreshKey;

        dispatch(startProfileAction());

        ConnectionService.standardWithAuth(
            Urls.PROFILE_PATH,
            {},
            refreshKey,
            res => {
                const errorCode = findError(res);

                if (errorCode) {
                    dispatch(errorProfileAction(errorCode));
                    return;
                }
                dispatch(successProfileAction(res.response.account));
            }
        )
    }
}

export function addHorseAsyncAction(horse) {
    return (dispatch, getState) => {
        const refreshKey = getState().data.refreshKey;

        dispatch(startAddHorseAction());

        ConnectionService.standardWithAuth(
            Urls.ADD_HORSE_PATH,
            {horse: horse},
            refreshKey,
            res => {
                const errorCode = findError(res);

                if (errorCode) {
                    dispatch(errorAddHorseAction(errorCode));
                    return;
                }
                dispatch(successAddHorseAction(res.response.horse));
            }
        )
    }
}

export function getHorsesAsyncAction() {
    return (dispatch, getState) => {
        const refreshKey = getState().data.refreshKey;

        dispatch(startGetHorsesAction());

        ConnectionService.standardWithAuth(
            Urls.GET_HORSES_PATH,
            {},
            refreshKey,
            res => {
                const errorCode = findError(res);

                if (errorCode) {
                    dispatch(errorGetHorsesAction(errorCode));
                    return;
                }
                dispatch(successGetHorsesAction(res.response.horses));
            }
        )
    }
}

export function startRaceAsyncAction(size) {

    return (dispatch, getState) => {
        dispatch(startStartRaceAction());

        ConnectionService.standard(
            Urls.START_RACE_PATH,
            {raceSize: size},
            res => {
                const errorCode = findError(res);

                if (errorCode) {
                    dispatch(errorStartRaceAction(errorCode));
                    return;
                }
                dispatch(successStartRaceAction(res.response.race));
            }
        )
    }
}