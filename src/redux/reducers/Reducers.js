import {combineReducers} from "redux";
import ReducersData from "./ReducersData";
import ReducersProcesses from "./ReducersProcesses";
import ReducersRace from "./ReducersRace";
import ReducersHorses from "./ReducersHorses";

export const Reduces = combineReducers({
    data: ReducersData,
    processes: ReducersProcesses,
    race: ReducersRace,
    horses: ReducersHorses,
});