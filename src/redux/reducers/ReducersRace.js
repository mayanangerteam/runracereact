import ActionKeys from "../../keys/ActionKeys";
import ActionStatusKeys from "../../keys/ActionStatusKeys";

function getInitialState() {
    return {
        race: null
    };
}

export default function (state = getInitialState(), action) {

    switch (action.type) {
        case ActionKeys.START_RACE_ACTION:
            if (action.status === ActionStatusKeys.SUCCESS) {
                return {...state, race: action.race}
            }
    }

    return state;
}