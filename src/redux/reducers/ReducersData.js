
import ActionKeys from "../../keys/ActionKeys";
import ActionStatusKeys from "../../keys/ActionStatusKeys";

function getInitialState() {
    return {
        account: null,
        accessKey: '',
        refreshKey: '',
        error: null,
    };
}

export default function (state = getInitialState(), action) {

    if (action.status === ActionStatusKeys.ERROR) {
        return {...state, error: action.error};
    }

    switch (action.type) {
        case ActionKeys.LOGIN_ACTION:
            if (action.status === ActionStatusKeys.SUCCESS) {
                return {...state, accessKey: action.accessKey, refreshKey: action.refreshKey};
            }
        case ActionKeys.PROFILE_ACTION:
            if (action.status === ActionStatusKeys.SUCCESS) {
                return {...state, account: action.account};
            }
        case ActionKeys.LOGOUT:
            return{...state, accessKey: null, refreshKey: null, account: null}

    }

    return state;
}