import ActionKeys from "../../keys/ActionKeys";
import ProcessKeys from "../../keys/ProcessKeys";
import ActionStatusKeys from "../../keys/ActionStatusKeys";

function getInitialState() {
    return {
        [ProcessKeys.REGISTER]:                 null,
        [ProcessKeys.LOGIN]:                    null,
        [ProcessKeys.ASK_RECOVER_PASSWORD]:     null,
        [ProcessKeys.APPROVE_RECOVER_PASSWORD]: null,
        [ProcessKeys.ADD_HORSE]:                null,
        [ProcessKeys.GET_HORSES]:               null,
        [ProcessKeys.START_RACE]:               null,
    };
}

export default function (state = getInitialState(), action) {

    switch (action.type) {

        case ActionKeys.REGISTER_ACTION:
            return {...state, [ProcessKeys.REGISTER]:                   action.status};

        case ActionKeys.LOGIN_ACTION:
            return {...state, [ProcessKeys.LOGIN]:                      action.status};

        case ActionKeys.PROFILE_ACTION:
            return {...state, [ProcessKeys.PROFILE]:                    action.status};

        case ActionKeys.ASK_RECOVER_PASSWORD_ACTION:
            return {...state, [ProcessKeys.ASK_RECOVER_PASSWORD]:       action.status};

        case ActionKeys.APPROVE_RECOVER_PASSWORD_ACTION:
            return {...state, [ProcessKeys.APPROVE_RECOVER_PASSWORD]:   action.status};

        case ActionKeys.GET_HORSES_ACTION:
            return {...state, [ProcessKeys.GET_HORSES]:                 action.status};

        case ActionKeys.ADD_HORSE_ACTION:
            return {...state, [ProcessKeys.ADD_HORSE]:                  action.status};

        case ActionKeys.START_RACE_ACTION:
            return {...state, [ProcessKeys.START_RACE]:                 action.status};
    }

    return state;
}