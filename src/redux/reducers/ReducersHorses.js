import ActionKeys from "../../keys/ActionKeys";
import ActionStatusKeys from "../../keys/ActionStatusKeys";

function getInitialState() {
    return {
        horses: [],
        addedHorse: null,
    };
}

export default function (state = getInitialState(), action) {

    switch (action.type) {
        case ActionKeys.ADD_HORSE_ACTION:
            if (action.status === ActionStatusKeys.SUCCESS) {
                return {...state, addedHorse: action.horse};
            }
        case ActionKeys.GET_HORSES_ACTION:
            if (action.status === ActionStatusKeys.SUCCESS) {
                return {...state, horses: action.horses};
            }
    }

    return state;
}