const PageKeys = {
    ENTRY: "entry",
    LOGIN: "login",
    MAIN: "main",
    APPROVE_RECOVER: "approverecover"
};

Object.freeze(PageKeys);

export default PageKeys;