const wrapperErrorCode = {
    CONNECTION_FAIL: "CONNECTION_FAIL",
    AUTH_FAIL: "AUTH_FAIL",
};

Object.freeze(wrapperErrorCode);

export default wrapperErrorCode;