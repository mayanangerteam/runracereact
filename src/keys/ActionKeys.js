const ActionKeys = {
    LOGIN_ACTION: "loginaction",
    REGISTER_ACTION: "registeraction",
    ASK_RECOVER_PASSWORD_ACTION: "askrecover",
    APPROVE_RECOVER_PASSWORD_ACTION: "approverecover",
    PROFILE_ACTION: "profileaction",
    ADD_HORSE_ACTION: "addhorse",
    GET_HORSES_ACTION: "gethorses",
    START_RACE_ACTION: "startrace",

    LOGOUT: "LOGOUT"
};

Object.freeze(ActionKeys);

export default ActionKeys;