const ActionStatusKeys = {
    START: "start",
    ERROR: "error",
    SUCCESS: "success",
};

Object.freeze(ActionStatusKeys);

export default ActionStatusKeys;