const ProcessKeys = {
    LOGIN: "login",
    REGISTER: "register",
    ASK_RECOVER_PASSWORD: "askrecover",
    APPROVE_RECOVER_PASSWORD: "approverecover",
    PROFILE: "profile",
    ADD_HORSE: "addhorse",
    GET_HORSES: "gethorses",
    START_RACE: "startrace",
};

Object.freeze(ProcessKeys);

export default ProcessKeys ;