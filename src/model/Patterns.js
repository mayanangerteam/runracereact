const PASSWORD_MAX = 20;
const PASSWORD_MIN = 6;

const REGULAR_PATTERN = "^[^@!\"']+$";
const PASSWORD_PATTERN = "^[^@!.\"']{"+PASSWORD_MIN+","+PASSWORD_MAX+"}$";
const EMAIL_PATTERN = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$";
const PHONE_NUMBER_PATTERN = "^(?:00|\\+?)[0-9]{6,20}$";

const patterns = {
    REGULAR_PATTERN: REGULAR_PATTERN,
    PASSWORD_PATTERN: PASSWORD_PATTERN,
    EMAIL_PATTERN: EMAIL_PATTERN,
    PHONE_NUMBER_PATTERN: PHONE_NUMBER_PATTERN,
};

export default patterns;