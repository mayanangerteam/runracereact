const status = {
    OK: "OK",
    ERROR: "ERROR",
};

Object.freeze(status);

export default status;