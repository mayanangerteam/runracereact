import Request from "../Request";

export default class ApproveRecoverPasswordRequest extends Request {
    email;
    newPassword;
    secretCode;
}