import Request from "../Request";

export default class AskRecoverPasswordRequest extends Request {
    email;
}