import Response from "../Response";

export default class AddHorseResponse extends Response {
    horse;
}