import AbstractModel from "./AbstractModel";

export default class Horse extends AbstractModel {
    nickname;
    race;
    account;
    privateName;
}