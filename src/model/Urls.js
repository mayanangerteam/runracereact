
const API_SEGMENT = "api";

const REGISTER_SEGMENT = "register";
const ASK_RECOVER_PASSWORD_SEGMENT = "askrecover";
const APPROVE_RECOVER_PASSWORD_SEGMENT = "approverecover";
const START_RACE_SEGMENT = "startrace";

const USER_SEGMENT = "user";

const PROFILE_SEGMENT = "profile";
const ADD_HORSE_SEGMENT = "addhorse";
const GET_HORSES_SEGMENT = "gethorses";

const urls = {
    REGISTER_PATH:                  API_SEGMENT + "/" + REGISTER_SEGMENT,
    ASK_RECOVER_PASSWORD_PATH:      API_SEGMENT + "/" + ASK_RECOVER_PASSWORD_SEGMENT,
    APPROVE_RECOVER_PASSWORD_PATH:  API_SEGMENT + "/" + APPROVE_RECOVER_PASSWORD_SEGMENT,
    START_RACE_PATH:                API_SEGMENT + "/" + START_RACE_SEGMENT,

    PROFILE_PATH:                   API_SEGMENT + "/" + USER_SEGMENT + "/" + PROFILE_SEGMENT,
    ADD_HORSE_PATH:                 API_SEGMENT + "/" + USER_SEGMENT + "/" + ADD_HORSE_SEGMENT,
    GET_HORSES_PATH:                API_SEGMENT + "/" + USER_SEGMENT + "/" + GET_HORSES_SEGMENT,
};

Object.freeze(urls);

export default urls;