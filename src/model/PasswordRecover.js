import AbstractModel from "./AbstractModel";

export default class PasswordRecover extends AbstractModel {
    account;
    secretCode;
    date;
    active;
}