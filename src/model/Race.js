import AbstractModel from "./AbstractModel";

export default class Race extends AbstractModel {
    horses;
    raceSteps;
    raceMap;
    date;
}