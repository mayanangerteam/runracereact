import AbstractModel from "./AbstractModel";

export default class RaceStep extends AbstractModel {
    horse1;
    horse2;
    horse3;
    horse4;
    horse5;
    horse6;
};