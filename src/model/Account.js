import AbstractModel from "./AbstractModel";

export default class Account extends AbstractModel {
    email;
    name;
    password;
}