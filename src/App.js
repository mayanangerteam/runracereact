import React, {Component} from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import {applyMiddleware, createStore} from "redux";
import {Provider} from "react-redux";
import {Reduces} from "./redux/reducers/Reducers";
import {composeWithDevTools} from "redux-devtools-extension";
import thunkMiddleware from "redux-thunk";
import ActorsService from "./services/ActorsService";
import {BrowserRouter, Link, Route, Switch} from "react-router-dom";
import EntryPage from "./components/pages/EntryPage";
import LoginPage from "./components/pages/LoginPage";
import MainPage from "./components/pages/MainPage";
import PageKeys from "./keys/PageKeys";
import ConnectionService from "./services/ConnectionService";
import Urls from "./model/Urls";
import AppBar from "@material-ui/core/es/AppBar/AppBar";
import Toolbar from "@material-ui/core/es/Toolbar/Toolbar";
import Typography from "@material-ui/core/es/Typography/Typography";
import Button from "@material-ui/core/es/Button/Button";
import createMuiTheme from "@material-ui/core/es/styles/createMuiTheme";
import i18n from "i18next";
import { withI18n, reactI18nextModule } from "react-i18next";
import en from "./resources/en.json";
import he from "./resources/he.json";
import MuiThemeProvider from "@material-ui/core/es/styles/MuiThemeProvider";
import ApproveRecoverPage from "./components/pages/ApproveRecoverPage";
import RunRaceBar from "./components/RunRaceBar";

// Theme setting
const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
});

// Translate setting
i18n
    .use(reactI18nextModule) // passes i18n down to react-i18next
    .init({
        resources: {
            en: {
                translation: en
            }
        },
        lng: "en",
        fallbackLng: "en",

        interpolation: {
            escapeValue: false
        }
    });

// Redux Setting
const store = createStore(
    Reduces,
    composeWithDevTools(
        applyMiddleware(
            thunkMiddleware,
            // loggerMiddleware,
        ))
);

// Link the ActorsService
store.subscribe(function() {
    let actor;
    while ( (actor = ActorsService.nextActor()) ) {
        actor(store.getState(), store.dispatch);
    }

});

const compStyle = theme => ({
    App: {
        textAlign: "center",
    },
    AppHeader: {
        backgroundColor: "#282c34",
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        fontSize: "calc(10px + 2vmin)",
        color: "white",
    },
    AppLink: {
        color: "#61dafb",
    },
    '@keyframes App-logo-spin': {
        from: { transform: "rotate(0deg)" },
        to : { transform: "rotate(360deg)" }
    },
    AppLogo: {
        animation: "App-logo-spin infinite 20s linear",
        height: "40vmin"
    },



});

class App extends Component {

    constructor(porps) {
        super(porps);
    }

    render() {

        const { classes } = this.props;

        return (
            <Provider store={store}>
                <BrowserRouter>
                    <MuiThemeProvider theme={theme}>
                        <div>

                            <img
                                src={require('./resources/background_blue.png')}
                                style={{position: "fixed", width: "100%", height: "100%", zIndex: -1000}}
                            />

                            <RunRaceBar/>

                            <Switch>
                                <Route path={"/" + PageKeys.MAIN } component={MainPage}/>
                                <Route path={"/" + PageKeys.LOGIN} component={LoginPage}/>
                                <Route path={"/" + PageKeys.APPROVE_RECOVER + "/:email"} component={ApproveRecoverPage}/>
                                {/*At last*/}
                                <Route path={"/"} component={EntryPage}/>
                            </Switch>

                        </div>
                    </MuiThemeProvider>
                </BrowserRouter>
            </Provider>
        );
    }
}

export default withStyles(compStyle)(App);