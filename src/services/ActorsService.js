
const _actors = [];

function nextActor() {
    if(_actors.length < 1) {
        return null;
    }
    const actor = _actors[0];
    _actors.shift();
    return actor;
}

function addActor(func) {
    _actors.push(func);
}

export default {
    nextActor,
    addActor
};