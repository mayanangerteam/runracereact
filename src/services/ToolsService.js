
import ErrorCode from "../model/connection/ErrorCode";

/**
 * search for an error in response
 * get a wrapper response.
 * search wrapper error. if isn't search if exists inner response and if there is a inner error != NO_ERROR
 * @param res a wrapper response
 * @return error from wrapper response or inner response
 */
export function findError(res) {
    if (res.errorCode) {
        return res.errorCode;
    }
    if (res.response && res.response.errorCode && res.response.errorCode !== ErrorCode.NO_ERROR) {
        return res.response.errorCode;
    }
}