import WrapperErrorCode from "../keys/WrapperErrorCode";

const BASE_URL = "http://runrae.us-east-1.elasticbeanstalk.com/";
const AUTHORIZATION_CODE = "Basic dXNlcjpydW5yYWNlLXNlY3JldA==";

function standardWithAuth(url, body, refreshKey, onDone) {
    getAccessKey(
        refreshKey,
        (res) => {
            // return from refresh
            if (res.errorCode) {
                onDone && onDone(res);
                return;
            }

            standard(
                url + "?access_token=" + res.response.access_token,
                body,
                onDone
            )
        }
    );
}

function standard(url, body, onDone) {
    nativeAutoJson(
        url,
        body,
        {},
        onDone,
    )
}

function auth(email, password, onDone) {
    nativeAutoJson(
        `oauth/token?grant_type=password&username=${email}&password=${password}`,
        undefined,
        {
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json",
            Authorization: AUTHORIZATION_CODE,
        },
        (res) => {
            if (res.errorCode) {
                onDone && onDone(res);
                return;
            }

            // We get the response
            if (res.nativeRes.ok) {
                onDone && onDone(res);
            } else {
                onDone && onDone({...res, errorCode: WrapperErrorCode.AUTH_FAIL});
            }
        }
    );
}

function getAccessKey(refreshKey, onDone) {
    nativeAutoJson(
        "oauth/token?grant_type=refresh_token&refresh_token=" + refreshKey, {}, {'Authorization': AUTHORIZATION_CODE},
        (res) => {

            if (res.errorCode) {
                // this request already failed. lets return the response at it is
                onDone && onDone(res);
                return;
            }

            if (res.response.error) {
                // refresh token failed
                onDone && onDone(res, WrapperErrorCode.AUTH_FAIL);
            } else {
                // success !
                onDone && onDone(res);
            }
        }
    )
}

function nativeAutoJson(url = '', body = {}, headers = {}, onDone) {
    native(
        url,
        body,
        headers,
        (res) => {
            // We have fine response, this is success
            if (!res.nativeRes) {
                onDone(res);
                return;
            }
            res.nativeRes.json().then(jsonBody => onDone({response: jsonBody, ...res}));
        }
    );
}

function native(url = '', body = {}, headers = {}, onDone) {
    fetch(BASE_URL + url, {
        method: "POST",
        headers: {
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json",
            ...headers
        },
        body: JSON.stringify(body)
    })
        .then(nativeRes => {
            // Done and get response
            if (401 < nativeRes.status && nativeRes.status < 500) {
                // Request didn't process right to the server
                onDone && onDone({nativeRes, errorCode: WrapperErrorCode.CONNECTION_FAIL});
            } else {
                // Success
                onDone && onDone({nativeRes});
            }
        })
        .catch(errorMessage => {
            // Cant connection to the server
            onDone && onDone({errorMessage, errorCode: WrapperErrorCode.CONNECTION_FAIL});
        });
}

export default {standardWithAuth, standard, native, nativeAutoJson, auth, getAccessKey};