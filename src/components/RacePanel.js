import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import PropTypes from "prop-types";
import HorseAnimated from "./HorseAnimated";

const compStyle = theme => ({

});

class RacePanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            refDetails: null,
            horsesDone: {},
            allDone: false,
        };
        this.ref = node => node && this.setState({refDetails: node.getBoundingClientRect()});
    }

    render() {

        const { race, classes } = this.props;

        const width = this.state.refDetails ? this.state.refDetails.width : 0;

        const size = 50;

        const horsesIdToStepLocationMap = this.getHorsesIdsList();

        const paramsIdMap = {};
        horsesIdToStepLocationMap.map((horseId, i) => {
            if (horseId !== null && horseId !== undefined) {
                paramsIdMap[horseId+""] = race.raceSteps.map(raceStep => raceStep["horse" + (i + 1)]);
            }
        });

        return (
            <div ref={this.ref} style={{width: "100%"}}>
                {Object.keys(paramsIdMap).map( (horseIdKey, i) => {
                    const currentParams = paramsIdMap[horseIdKey];
                    const jumpWidth = (width - size) / currentParams.length;
                    return (
                        <div key={i}>
                            <HorseAnimated onDoneRoute={() => this.horseDone(i)} params={currentParams} jump={jumpWidth} size={size} />
                        </div>);
                } )}
            </div>
        );
    }

    horseDone(i) {
        this.setState({horsesDone: {[(i+1).toString()]: true, ...this.state.horsesDone}});
    }

    componentWillUpdate(nextProps, nextState) {
        if (!this.state.allDone && nextState.allDone) {
            this.props.allDone && this.props.allDone();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.horsesDone) {
            const idList = this.getHorsesIdsList();
            let allDone = true;
            for (let i = 0; i < idList.length; i++) {
                if (idList[i] && !this.state.horsesDone[(i+1).toString()]) {
                    allDone = false;
                    break;
                }
            }

            if (allDone) {
                if (!this.state.allDone) {
                    this.setState({allDone: true});
                }
            }
        }
    }

    getHorsesIdsList() {
        return [
            this.props.race.raceMap.horseId1,
            this.props.race.raceMap.horseId2,
            this.props.race.raceMap.horseId3,
            this.props.race.raceMap.horseId4,
            this.props.race.raceMap.horseId5,
            this.props.race.raceMap.horseId6,
        ];
    }

}

RacePanel.propTypes = {
    race: PropTypes.object.isRequired,
    allDone: PropTypes.func,
};

RacePanel.defaultProps = {
    size: 20
};

export default withStyles(compStyle)(RacePanel);