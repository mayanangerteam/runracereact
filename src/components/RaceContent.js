import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Grid from "@material-ui/core/es/Grid/Grid";
import {withI18n} from "react-i18next";
import withRouter from "react-router-dom/es/withRouter";
import connect from "react-redux/es/connect/connect";
import Button from "@material-ui/core/es/Button/Button";
import ProcessKeys from "../keys/ProcessKeys";
import Slider from "rc-slider";
import Typography from "@material-ui/core/es/Typography/Typography";
import {startRaceAsyncAction} from "../redux/actions/AsyncAction";
import ActionStatusKeys from "../keys/ActionStatusKeys";
import {logoutAction} from "../redux/actions/Actions";
import RacePanel from "./RacePanel";
import Table from "@material-ui/core/es/Table/Table";
import TableHead from "@material-ui/core/es/TableHead/TableHead";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import TableBody from "@material-ui/core/es/TableBody/TableBody";
import PropTypes from "prop-types";

const compStyle = theme => ({
    slider: {
        paddingTop: theme.spacing.unit * 3,
        paddingBottom: theme.spacing.unit * 3,
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
    },
    mar: {
        margin: theme.spacing.unit * 2
    }
});

class RaceContent extends React.Component {

    static Modes = {
        START_FORM: "start",
        ONGOING_RACE: "ongoing",
        AFTER_RACE: "afterrace",
        ERROR: "error",
    };

    constructor(props) {
        super(props);
        this.state = {
            sizeValue: 2,
            onProcess: false,
            race: null,
            mode: RaceContent.Modes.START_FORM,
            raceDone: false,
        }
    }

    render() {
        const { classes, t } = this.props;
        const { mode } = this.state;

        return (
            <div>
                <Grid container justify={"center"} alignItems={"center"} direction={"column"}>

                    {mode === RaceContent.Modes.START_FORM ?
                        <Grid item xs>

                            <div className={classes.slider}>
                                <Slider min={2} max={6} value={this.state.sizeValue}
                                        onChange={(value) => this.setState({sizeValue: value})}/>
                            </div>
                            <div style={{textAlign: "center"}}>
                        <span>
                            {this.state.sizeValue + " : " + t("GENERAL.HORSES")}
                        </span>
                            </div>
                            <div>
                                <Button
                                    className={classes.mar}
                                    disabled={this.state.onProcess}
                                    color="primary"
                                    variant={"contained"}
                                    onClick={() => this.startRace()}>
                                    {t("GENERAL.START_RACE")}
                                </Button>
                            </div>

                        </Grid> : null}

                    {mode === RaceContent.Modes.ERROR ?
                        <Grid item xs>
                            <Typography variant={"h3"}>
                                {this.props.error ? t("MESSAGES.FAILED_RACE") : null}
                            </Typography>

                            <Typography>
                                {this.props.error ? t("MESSAGES." + t(this.props.error)) : null}
                            </Typography>

                        </Grid> : null}

                </Grid>

                <div style={{width: "100%"}}>
                    {mode === RaceContent.Modes.ONGOING_RACE ?
                        <div>
                            <RacePanel race={this.props.race} allDone={() => this.setState({raceDone: true})}/>
                            <div style={{textAlign: "center", width: "100%"}}>
                                <Button
                                    className={classes.mar}
                                    color="primary"
                                    variant={"contained"}
                                    disabled={!this.state.raceDone}
                                    onClick={() => this.setState({mode: RaceContent.Modes.AFTER_RACE})}>
                                    {t("GENERAL.CONTINUE")}
                                </Button>
                            </div>
                        </div>
                        : null}

                    {mode === RaceContent.Modes.AFTER_RACE?
                        <div style={{textAlign: "center", width: "100%"}}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <TableCell numeric>{t("GENERAL.TOURNAMENT_NUMBER")}</TableCell>
                                        <TableCell>{t("HORSE.NICKNAME")}</TableCell>
                                        <TableCell numeric>{t("GENERAL.TIME_TAKEN")}</TableCell>
                                        <TableCell numeric>{t("GENERAL.RACE_PLACE")}</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {this.getRows().map(row => {
                                        return (
                                            <TableRow key={row.id}>
                                                <TableCell >{row.raceNumber}</TableCell>
                                                <TableCell numeric>{row.horseName}</TableCell>
                                                <TableCell numeric>{row.timeSum}</TableCell>
                                                <TableCell component="th" scope="row">{row.place}</TableCell>
                                            </TableRow>
                                        );
                                    })}
                                </TableBody>
                            </Table>
                            <Button
                                className={classes.mar}
                                color="primary"
                                variant={"contained"}
                                onClick={() => this.props.closeClicked && this.props.closeClicked() }>
                                {t("GENERAL.DONE")}
                            </Button>
                        </div>
                        : null}
                </div>

            </div>
        );
    }

    getRows() {
        const rows = [];
        for (let i =0 ; i<6; i++) {
            const horseId = this.props.race.raceMap["horseId" + (i+1)];
            if (!horseId) {
                continue;
            }

            const horse = this.props.race.horses.find(horse => horse.id === horseId);

            const timeSum = this.props.race.raceSteps.map(raceStep => {
                return raceStep["horse" + (i + 1)];
            }).reduce((sum = 0, item) => (sum + item));
            rows.push({id: horseId, timeSum: timeSum, raceNumber: i, horseName: horse.nickname});
        }

        rows.sort((r1, r2) => r1.timeSum - r2.timeSum);
        rows.forEach((row, i) => row.place = i);

        return rows;
    }

    startRace() {
        this.props.startRaceAction(this.state.sizeValue);
    }

    componentWillReceiveProps(nextProps) {
        const raceProcess = nextProps.raceProcess;
        const onProcess = raceProcess === ActionStatusKeys.START;

        const raceChanged = raceProcess !== this.props.raceProcess;

        let mode = this.state.mode;

        if (raceChanged && this.props.raceProcess === ActionStatusKeys.START) {
            if (raceProcess === ActionStatusKeys.ERROR) {
                mode = RaceContent.Modes.ERROR;
            } else if (raceProcess === ActionStatusKeys.SUCCESS) {
                // TODO set here time start of game
                mode = RaceContent.Modes.ONGOING_RACE;
            }
        }

        // TODO find if set set auto check if state props changed
        if (this.state.onProcess !== onProcess ||
            this.state.mode !== mode) {
            this.setState({
                onProcess: onProcess,
                mode: mode,
            });
        }
    }
}

RaceContent.propTypes = {
    closeClicked: PropTypes.func
};

function mapStateToProps(state) {
    return {
        ...state.data,
        raceProcess: state.processes[ProcessKeys.START_RACE],
        race: state.race.race,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        startRaceAction: (size) => dispatch(startRaceAsyncAction(size)),
        logoutAction: () => dispatch(logoutAction())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(RaceContent))));