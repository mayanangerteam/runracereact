import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Grid from "@material-ui/core/es/Grid/Grid";
import Paper from "@material-ui/core/es/Paper/Paper";
import AppBar from "@material-ui/core/es/AppBar/AppBar";
import Toolbar from "@material-ui/core/es/Toolbar/Toolbar";
import Typography from "@material-ui/core/es/Typography/Typography";
import Button from "@material-ui/core/es/Button/Button";
import {withI18n} from "react-i18next";
import withRouter from "react-router-dom/es/withRouter";
import connect from "react-redux/es/connect/connect";
import {logoutAction, successLoginAction} from "../redux/actions/Actions";
import {withCookies} from "react-cookie";

const compStyle = theme => ({
    appBar: {
        position: 'relative',
    },
    toolbarTitle: {
        flex: 1,
    }
});

class RunRaceBar extends React.Component {
    render() {

        const { classes } = this.props;

        const {t} = this.props;

        return (
            <AppBar position="static" color="default" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                        {t("TITLE.RUNRACE_TITLE")}
                    </Typography>
                    {this.props.refreshKey ?
                        (<Button onClick={() => this.onLogoutClick()} color="primary" variant="outlined">
                            {t("GENERAL.LOGOUT")}
                        </Button>) : null}
                </Toolbar>
            </AppBar>
        );
    }

    componentWillReceiveProps(nextProps) {
        const cookieRefreshKey = this.props.cookies.get("refreshKey");
        const nextRefreshKey = nextProps.refreshKey;

        if (cookieRefreshKey !== nextRefreshKey && (cookieRefreshKey || nextRefreshKey)) {
            const tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            this.props.cookies.set("refreshKey", nextRefreshKey ? nextRefreshKey : "", {expires: tomorrow});
        }
    }

    componentDidMount() {
        if (!this.props.refreshKey) {
            const rk = this.props.cookies.get("refreshKey");
            if (rk) {
                this.props.setRefreshKey(rk);
            }
        }
    }

    onLogoutClick() {
        this.props.logout();
    }
}

function mapStateToProps(state) {
    return {
        refreshKey: state.data.refreshKey,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout: () => dispatch(logoutAction()),
        setRefreshKey: (refreshKey) => dispatch(successLoginAction(refreshKey, null))
    }
}

export default withRouter(withCookies(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(RunRaceBar)))));