import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import styled, {keyframes} from "styled-components";
import PropTypes from "prop-types";

const compStyle = theme => ({

});

const horsesImages = [
    [
        require("./../resources/horses/horse1_1.png"),
        require("./../resources/horses/horse1_2.png"),
        require("./../resources/horses/horse1_3.png"),
        require("./../resources/horses/horse1_4.png"),
    ],
    [
        require("./../resources/horses/horse2_1.png"),
        require("./../resources/horses/horse2_2.png"),
        require("./../resources/horses/horse2_3.png"),
        require("./../resources/horses/horse2_4.png"),
    ],
    [
        require("./../resources/horses/horse3_1.png"),
        require("./../resources/horses/horse3_2.png"),
        require("./../resources/horses/horse3_3.png"),
        require("./../resources/horses/horse3_4.png"),
    ],
];

class HorseAnimated extends React.Component {

    static IdCount = 0;

    constructor(props) {
        super(props);
        this.id = HorseAnimated.IdCount++;
    }

    render() {
        const { params, size, jump} = this.props;
        let horseMode = this.props.horseMode;
        if(!horseMode) {
            horseMode = Math.floor(Math.random() * Math.floor(3));
        }

        const sum = params.reduce((accumulator, currentValue) => accumulator + currentValue);
        const percentUnit = sum / 100;

        let keyframesValue = "";
        let i = 0;
        let percentsSum = 0;
        for (let timeValue of params) {
            percentsSum += timeValue / percentUnit;
            i++;
            keyframesValue += `${Math.round(percentsSum)}%   {transform: translateX(${i * jump}px  ) } `;
        }
        const horseAnimatedDuration = 500;


        const keyframe = keyframes`${keyframesValue}`;
        const Move = styled.div`
            animation: ${keyframe} ${sum}ms 0s linear forwards; 
            display: flex;
            padding: ${size/10}px
            width: ${size}px; 
            height: ${size}px; 
            background-color: transparent;
        `;


        const horseKeyframeValue = `
            0%   {background-image: url(${horsesImages[horseMode][0]})}
            25%  {background-image: url(${horsesImages[horseMode][1]})}
            50%  {background-image: url(${horsesImages[horseMode][2]})}
            100% {background-image: url(${horsesImages[horseMode][3]})}
        `;

        const horseKeyframe = keyframes`${horseKeyframeValue}`;
        const Horse = styled.div`
            animation: ${horseKeyframe} ${horseAnimatedDuration}ms linear;
            animation-iteration-count: ${sum / horseAnimatedDuration};
            background-size: cover; 
            background-image: url(${horsesImages[horseMode][3]});
            flex: 1;
            background-position: center;
        `;

        return (
            <Move>
                <Horse id={"move-horse-root-" + this.id}/>
            </Move>
        );
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (this.props.size !== nextProps.size ||
                this.props.jump !== nextProps.jump ||
                JSON.stringify(this.props.params) !== JSON.stringify(nextProps.params) ||
                this.props.horseMode !== nextProps.horseMode
        );
    }

    componentDidMount() {
        this.setOnDoneListener();
    };

    componentDidUpdate() {
        this.setOnDoneListener();
    }

    setOnDoneListener = () => {
        const element = document.getElementById("move-horse-root-" + this.id);
        element.addEventListener('animationend', () => {
            this.props.onDoneRoute && this.props.onDoneRoute();
        });
    };
}

HorseAnimated.propTypes = {
    jump: PropTypes.number.isRequired,
    params: PropTypes.arrayOf(PropTypes.number).isRequired,
    size: PropTypes.number.isRequired,
    horseMode: PropTypes.number,
    onDoneRoute: PropTypes.func
};

export default withStyles(compStyle)(HorseAnimated);