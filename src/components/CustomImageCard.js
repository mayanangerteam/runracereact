import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Card from "@material-ui/core/es/Card/Card";
import CardActionArea from "@material-ui/core/es/CardActionArea/CardActionArea";
import CardMedia from "@material-ui/core/es/CardMedia/CardMedia";
import CardContent from "@material-ui/core/es/CardContent/CardContent";
import Typography from "@material-ui/core/es/Typography/Typography";
import CardActions from "@material-ui/core/es/CardActions/CardActions";
import Button from "@material-ui/core/es/Button/Button";
import PropTypes from "prop-types";

const compStyle = theme => ({

});

class CustomImageCard extends React.Component {
    render() {

        const { classes } = this.props;

        return (
            <Card className={classes.card}>
                <CardActionArea disabled={this.props.disabled}>
                    <CardMedia
                        style={this.props.disabled ? {filter: "grayscale(100%)"} : {}}
                        component="img"
                        alt={this.props.alt}
                        className={classes.media}
                        image={this.props.image}
                        title={this.props.title}
                        onClick={() => this.props.imageOnClick && this.props.imageOnClick()}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.props.title}
                        </Typography>
                        <Typography component="p">
                            {this.props.content}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    {this.props.buttonText ? (
                        <Button disabled={this.props.disabled} size="small" color="primary" onClick={() => this.props.buttonOnClick && this.props.buttonOnClick()}>
                            {this.props.buttonText}
                        </Button>
                    ) : null}
                </CardActions>
            </Card>
        );
    }
}

CustomImageCard.propTypes = {
    image: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
    title: PropTypes.string,
    content: PropTypes.string,
    alt: PropTypes.string,
    imageOnClick: PropTypes.func,
    buttonOnClick: PropTypes.func,
    disabled: PropTypes.bool,
    buttonText: PropTypes.string,
};

export default withStyles(compStyle)(CustomImageCard);