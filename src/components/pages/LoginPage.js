import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import {BrowserRouter, Link, Route, withRouter} from "react-router-dom";
import PageKeys from "../../keys/PageKeys";
import MainPage from "./MainPage";
import EntryPage from "./EntryPage";
import Grid from "@material-ui/core/es/Grid/Grid";
import Paper from "@material-ui/core/es/Paper/Paper";
import TextField from "@material-ui/core/es/TextField/TextField";
import {withI18n} from "react-i18next";
import Button from "@material-ui/core/es/Button/Button";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/es/Checkbox/Checkbox";
import Typography from "@material-ui/core/es/Typography/Typography";
import {connect} from "react-redux";
import {askRecoverAsyncAction, loginAsyncAction, registerAsyncAction} from "../../redux/actions/AsyncAction";
import Patterns from "../../model/Patterns";
import ProcessKeys from "../../keys/ProcessKeys";
import ActionStatusKeys from "../../keys/ActionStatusKeys";
import Dialog from "@material-ui/core/es/Dialog/Dialog";
import DialogTitle from "@material-ui/core/es/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/es/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/es/DialogContentText/DialogContentText";
import DialogActions from "@material-ui/core/es/DialogActions/DialogActions";
import MessageDialog from "../MessageDialog";
import Redirect from "react-router-dom/es/Redirect";
import StartRaceRequest from "../../model/connection/request/api/StartRaceRequest";
import RegisterRequest from "../../model/connection/request/api/RegisterRequest";

const compStyle = theme => ({
    mar: {
        margin: theme.spacing.unit * 2
    },
    container: {
        padding: theme.spacing.unit * 6
    },
    marTop: {
        marginTop: 100
    }
});


class LoginPage extends React.Component {

    static LoginPageModes = {
        REGISTER: "register",
        LOGIN: "login",
        FORGOT_PASSWORD: "forgotpassword",
    };

    constructor(props) {
        super(props);
        this.state = {
            emailValue: "",
            passwordValue: "",
            registerOn: false,
            forgotPasswordOn: false,
            isClicked: false,
            onProcess: false,
            moveMainPage: false,
            loginModal: false,
            registerModal: false,
            askRecoverModal: false,
            moveApproveRecoverPage: false,
        }
    }

    render() {

        const { classes } = this.props;
        const mode = this.getMode();

        const passwordOk =  this.passwordOk();
        const emailOk =     this.emailOk();

        const t = this.props.t;

        const showPasswordError = this.state.isClicked && !passwordOk && (!mode === LoginPage.LoginPageModes.REGISTER);

        const askRecoverProcessOnErrorMode = this.props.askRecoverProcess === ActionStatusKeys.ERROR;

        return (
            <Grid container justify={"center"}>
                <Grid item>

                    {this.props.refreshKey || this.state.moveMainPage ? <Redirect to={"/" + PageKeys.MAIN}/> : null}
                    {this.state.moveApproveRecoverPage ? <Redirect to={"/" + PageKeys.APPROVE_RECOVER + "/" + this.state.emailValue }/> : null}

                    <MessageDialog
                        open={this.state.loginModal}
                        color={"error"}
                        title={t("MESSAGES.LOGIN_FAIL")}
                        content={this.props.error ? t("MESSAGES." + this.props.error.toUpperCase()) : ""}
                        onClickAction={() => this.setState({loginModal: false})}
                        buttonText={t("GENERAL.BACK")}/>

                    <MessageDialog
                        open={this.state.registerModal}
                        color={this.props.registerProcess === ActionStatusKeys.ERROR ? "error" : "primary"}
                        title={t(this.props.registerProcess === ActionStatusKeys.ERROR ? "MESSAGES.REGISTER_FAIL" : "MESSAGES.REGISTER_SUCCESS")}
                        content={t(
                            this.props.registerProcess === ActionStatusKeys.ERROR ?
                                (this.props.error ? "MESSAGES." + this.props.error.toUpperCase() : "")
                                : "MESSAGES.REGISTER_SUCCESS_MESSAGE" )}
                        onClickAction={() => this.setState({registerModal: false})}
                        buttonText={t("GENERAL.BACK")}/>

                    <MessageDialog
                        open={this.state.askRecoverModal}
                        color={askRecoverProcessOnErrorMode ? "error" : "primary"}
                        title={t(askRecoverProcessOnErrorMode ? "MESSAGES.ASK_RECOVER_FAIL" : "MESSAGES.ASK_RECOVER_SUCCESS")}
                        content={t(
                            askRecoverProcessOnErrorMode ?
                                (this.props.error ? "MESSAGES." + this.props.error.toUpperCase() : "")
                                : "MESSAGES.ASK_RECOVER_SUCCESS_MESSAGE" )}
                        onClickAction={() => this.setState({askRecoverModal: false, moveApproveRecoverPage: !askRecoverProcessOnErrorMode })}
                        buttonText={t(askRecoverProcessOnErrorMode ? "GENERAL.BACK" : "GENERAL.CONTINUE")}/>

                    {/*TODO what if app color mode on dark mod ? */}
                    <Paper elevation={10} className={`${classes.container} ${classes.marTop}`} style={{backgroundColor: "#ffffff99"}}>

                        <Grid container justify={"space-around"} direction={"column"} alignItems={"center"}>

                            <Grid item>
                                <Typography
                                    className={classes.mar}
                                    variant={"h3"}
                                    color={this.state.registerOn ? "secondary" : "primary"}>
                                    {this.props.t(this.state.registerOn ?  "GENERAL.REGISTER" : "GENERAL.LOGIN")}
                                </Typography>
                            </Grid>

                            <Grid item>
                                <TextField
                                    error={this.state.isClicked && !emailOk}
                                    id="email-input"
                                    label={this.props.t("USER.EMAIL")}
                                    type="email"
                                    name="email"
                                    autoComplete="email"
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.emailValue}
                                    onChange={(event) => this.setState({emailValue: event.target.value})}
                                />
                            </Grid>

                            <Grid item>
                                <TextField
                                    error={showPasswordError}
                                    id="password-input"
                                    label={this.props.t("USER.PASSWORD")}
                                    type="password"
                                    autoComplete="current-password"
                                    margin="normal"
                                    variant="outlined"
                                    disabled={mode === LoginPage.LoginPageModes.FORGOT_PASSWORD}
                                    value={this.state.passwordValue}
                                    onChange={(event) => this.setState({passwordValue: event.target.value})}
                                />
                            </Grid>

                            <Grid item className={classes.mar}>
                                <Button
                                    variant={"contained"}
                                    color="primary"
                                    disabled={this.state.onProcess}
                                    onClick={() => this.onDoneClick()}>
                                    {this.props.t({
                                        [LoginPage.LoginPageModes.FORGOT_PASSWORD]: "GENERAL.FORGOT_PASSWORD",
                                        [LoginPage.LoginPageModes.LOGIN]: "GENERAL.LOGIN",
                                        [LoginPage.LoginPageModes.REGISTER]: "GENERAL.JOIN"
                                    }[mode])}
                                </Button>
                            </Grid>

                            <Grid item className={classes.mar}>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.forgotPasswordOn}
                                            onChange={(event, newCheckValue) => this.toggleForgotPasswordFlag()}
                                            value="forgotPassword"
                                            disabled={this.state.registerOn}
                                        />
                                    }
                                    label={this.props.t("GENERAL.FORGOT_PASSWORD")}
                                />
                            </Grid>

                            <Grid item className={classes.mar}>
                                <Button
                                    variant={"outlined"}
                                    color={this.state.registerOn ? "primary" : "secondary"}
                                    onClick={() => this.toggleRegisterFlag()}>
                                    {this.props.t(this.state.registerOn ? "LOGIN_PAGE.TO_LOGIN" : "LOGIN_PAGE.TO_REGISTER")}
                                </Button>
                            </Grid>

                            <Grid item
                                  className={classes.mar}
                                  style={{opacity: showPasswordError ? 1 : 0}}>
                                <Typography variant={"body1"} color="error">
                                    {this.props.t("MESSAGES.PASSWORD_ERROR")}
                                </Typography>
                            </Grid>



                        </Grid>

                    </Paper>

                </Grid>
            </Grid>
        );
    }

    /**
     * @return string, the current mode by mode keys. use the state params
     */
    getMode() {
        if (this.state.registerOn) {
            return LoginPage.LoginPageModes.REGISTER;
        }
        if (this.state.forgotPasswordOn) {
            return LoginPage.LoginPageModes.FORGOT_PASSWORD;
        }
        return LoginPage.LoginPageModes.LOGIN;
    }

    toggleRegisterFlag() {
        this.setState({
            registerOn: !this.state.registerOn
        });
    }

    toggleForgotPasswordFlag() {
        this.setState({
            forgotPasswordOn: !this.state.forgotPasswordOn
        });
    }

    /**
     * Register / Login / Forgot Password
     */
    onDoneClick() {
        if (!this.state.isClicked) {
            this.setState({isClicked: true});
        }

        const mode = this.getMode();
        switch (mode) {
            case LoginPage.LoginPageModes.FORGOT_PASSWORD:
                return this.onForgotPassword();
            case LoginPage.LoginPageModes.REGISTER:
                return this.onRegister();
            case LoginPage.LoginPageModes.LOGIN:
                return this.onLogin();
        }
    }

    onForgotPassword() {
        if (!this.emailOk()) {
            return;
        }
        this.props.askRecoverPassword(this.state.emailValue);
    }

    onRegister() {
        if (!(this.passwordOk() && this.emailOk())) {
            return;
        }
        this.props.registerAction(this.state.emailValue, this.state.passwordValue);
    }

    onLogin() {
        if (!(this.passwordOk() && this.emailOk())) {
            return;
        }
        this.props.loginAction(this.state.emailValue, this.state.passwordValue);
    }

    passwordOk() {
        return new RegExp(Patterns.PASSWORD_PATTERN).test(this.state.passwordValue);
    }

    emailOk() {
        return new RegExp(Patterns.EMAIL_PATTERN).test(this.state.emailValue);
    }

    componentWillReceiveProps(nextProps) {
        const loginProcess = nextProps.loginProcess;
        const registerProcess = nextProps.registerProcess;
        const askRecoverProcess = nextProps.askRecoverProcess;
        const onProcess =
            loginProcess === ActionStatusKeys.START ||
            registerProcess === ActionStatusKeys.START ||
            askRecoverProcess === ActionStatusKeys.START;

        const loginChanged = loginProcess !== this.props.loginProcess;
        const registerChanged = registerProcess !== this.props.registerProcess;
        const askRecoverChanged = askRecoverProcess !== this.props.askRecoverProcess;

        const loginModal = loginChanged && loginProcess === ActionStatusKeys.ERROR && this.props.loginProcess === ActionStatusKeys.START;
        const registerModal = registerChanged && registerProcess !== ActionStatusKeys.START && this.props.registerProcess === ActionStatusKeys.START;
        const askRecoverModal = askRecoverChanged && askRecoverProcess !== ActionStatusKeys.START && this.props.askRecoverProcess === ActionStatusKeys.START;

        const moveMainPage = loginChanged && loginProcess === ActionStatusKeys.SUCCESS && this.props.loginProcess === ActionStatusKeys.START;

        // TODO find if set set auto check if state props changed
        if (this.state.onProcess !== onProcess ||
            this.state.loginModal !== loginModal ||
            this.state.registerModal !== registerModal ||
            this.state.askRecoverModal !== askRecoverModal ||
            this.state.moveMainPage !== moveMainPage) {
            this.setState({
                onProcess: onProcess,
                loginModal: loginModal,
                registerModal: registerModal,
                askRecoverModal: askRecoverModal,
                moveMainPage: moveMainPage,
            });
        }

    }

}

function mapStateToProps(state) {
    return {
        loginProcess: state.processes[ProcessKeys.LOGIN],
        registerProcess: state.processes[ProcessKeys.REGISTER],
        askRecoverProcess: state.processes[ProcessKeys.ASK_RECOVER_PASSWORD],
        error: state.data.error,
        refreshKey: state.data.refreshKey,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        registerAction: (email, password) => dispatch(registerAsyncAction(email, password)),
        loginAction: (email, password) => dispatch(loginAsyncAction(email, password)),
        askRecoverPassword: (email) => dispatch(askRecoverAsyncAction(email)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(LoginPage))));