import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import {Redirect, withRouter} from "react-router-dom";
import PageKeys from "../../keys/PageKeys";
import {connect} from "react-redux";
import Grid from "@material-ui/core/es/Grid/Grid";
import {withI18n} from "react-i18next";
import CustomImageCard from "../CustomImageCard";
import Dialog from "@material-ui/core/es/Dialog/Dialog";
import DialogContent from "@material-ui/core/es/DialogContent/DialogContent";
import RaceContent from "../RaceContent";
import IconButton from "@material-ui/core/es/IconButton/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import DialogTitle from "@material-ui/core/es/DialogTitle/DialogTitle";
import AddHorseContent from "../AddHorseContent";
import ManageHorsesContent from "../ManageHorsesContent";

const compStyle = theme => ({
    darkBackground: {
        backgroundColor: theme.palette.grey[800],
    },
    mar: {
        margin: theme.spacing.unit * 2
    }
});

class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            moveToLogin: false,
            openRaceDialog: false,
            openAddHorseDialog: false,
            openManageHorseDialog: false,
        };
    }

    render() {

        const { classes } = this.props;
        const {t} = this.props;

        const itemXs = 10;
        const itemMd = 6;
        const itemLg = 4;

        return (
            <Grid container justify={"space-around"} alignItems={"center"} direction={"column"}>

                {this.state.moveToLogin ? <Redirect to={"/" + PageKeys.LOGIN}/> : null}

                <Dialog fullWidth={true}
                    open={this.state.openRaceDialog}
                    aria-labelledby="scroll-dialog-title">
                    <DialogTitle id="simple-dialog-title" className={classes.darkBackground}>
                        <div>
                            <IconButton aria-label="Close" onClick={() => this.closeRaceDialog()}>
                                <CloseIcon color={"secondary"}/>
                            </IconButton>
                        </div>
                    </DialogTitle>
                    <DialogContent>
                        <RaceContent closeClicked={() => this.closeRaceDialog()}/>
                    </DialogContent>
                </Dialog>

                <Dialog
                        open={this.state.openAddHorseDialog}
                        aria-labelledby="scroll-dialog-title">
                    <DialogTitle id="simple-dialog-title" className={classes.darkBackground}>
                        <div>
                            <IconButton aria-label="Close" onClick={() => this.setState({openAddHorseDialog: false})}>
                                <CloseIcon color={"secondary"}/>
                            </IconButton>
                        </div>
                    </DialogTitle>
                    <DialogContent>
                        <AddHorseContent/>
                    </DialogContent>
                </Dialog>

                <Dialog
                    open={this.state.openManageHorseDialog}
                    aria-labelledby="scroll-dialog-title">
                    <DialogTitle id="simple-dialog-title" className={classes.darkBackground}>
                        <div>
                            <IconButton aria-label="Close" onClick={() => this.setState({openManageHorseDialog: false})}>
                                <CloseIcon color={"secondary"}/>
                            </IconButton>
                        </div>
                    </DialogTitle>
                    <DialogContent>
                        <ManageHorsesContent/>
                    </DialogContent>
                </Dialog>

                <Grid item xs={itemXs} md={itemMd} lg={itemLg} className={classes.mar}>

                    <CustomImageCard
                        title={t("CONTENT.START_RACE")}
                        content={t("CONTENT.START_RACE_CONTENT")}
                        alt={t("CONTENT.START_RACE")}
                        image={require("./../../resources/horses_race.jpg")}
                        buttonText={t("CONTENT.START_RACE")}
                        imageOnClick={() => this.openRaceDialog()}
                        buttonOnClick={() => this.openRaceDialog()}
                    />

                </Grid>

                <Grid item xs={itemXs} md={itemMd} lg={itemLg} className={classes.mar}>

                    <CustomImageCard
                        title={t("CONTENT.ADD_HORSE")}
                        content={t("CONTENT.ADD_HORSE_CONTENT")}
                        alt={t("CONTENT.ADD_HORSE")}
                        image={require("./../../resources/add_horse.jpg")}
                        disabled={!this.props.refreshKey}
                        buttonText={t("CONTENT.ADD_HORSE")}
                        imageOnClick={() => this.openAddHorseDialog()}
                        buttonOnClick={() => this.openAddHorseDialog()}
                    />

                </Grid>

                <Grid item xs={itemXs} md={itemMd} lg={itemLg} className={classes.mar}>

                    <CustomImageCard
                        title={t("CONTENT.MANAGE_HORSES")}
                        content={t("CONTENT.MANAGE_HORSES_CONTENT")}
                        alt={t("CONTENT.MANAGE_HORSES")}
                        image={require("./../../resources/manage_horses.jpg")}
                        disabled={!this.props.refreshKey}
                        buttonText={t("CONTENT.MANAGE_HORSES")}
                        imageOnClick={() => this.openManageDialog()}
                        buttonOnClick={() => this.openManageDialog()}
                    />

                </Grid>

            </Grid>
        );
    }

    componentWillReceiveProps(nextProps) {
        const refreshKey = nextProps.refreshKey;
        const refreshKeyPrev = this.props.refreshKey;
        if (refreshKey === null && refreshKeyPrev) {
            // meaning, logout happen
            this.setState({
                moveToLogin: true
            });
        }
    }

    closeRaceDialog() {
        return this.setState({openRaceDialog: false});
    }

    openRaceDialog() {
        this.setState({openRaceDialog: true});
    }

    openAddHorseDialog() {
        this.setState({openAddHorseDialog: true});
    }

    openManageDialog() {
        this.setState({openManageHorseDialog: true});
    }

}

function mapStateToProps(state) {
    return {
        ...state.data
    }
}

function mapDispatchToProps(dispatch) {
    return {

    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(MainPage))));
