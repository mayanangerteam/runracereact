import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Grid from "@material-ui/core/es/Grid/Grid";
import CardPoints from "../CardPoints";
import {isWidthDown} from "@material-ui/core/es/withWidth/withWidth";
import {withI18n} from "react-i18next";
import PageKeys from "../../keys/PageKeys";
import Redirect from "react-router-dom/es/Redirect";
import withRouter from "react-router-dom/es/withRouter";
import connect from "react-redux/es/connect/connect";


const compStyle = theme => ({
    mar: {
        margin: theme.spacing.unit * 2
    }
});

class EntryPage extends React.Component {
    render() {

        const { t } = this.props;
        const { classes } = this.props;
        const width = this.props.width;
        const smallMode = isWidthDown("sm", width, true);
        // const direction = smallMode  ? "column" : "row";

        return (
            <Grid container alignItems={"center"} justify={"space-around"} style={{marginTop: smallMode ? 0 : 150}}>

                {this.props.refreshKey ? <Redirect to={"/" + PageKeys.MAIN}/> : null}

                <Grid item xs={8} md={3} className={classes.mar}>
                    <CardPoints
                        title={t("GENERAL.GUSSET")}
                        subTitle={t("ENTRY_PAGE.ENTER_AS_GUSSET")}
                        buttonTitle={t("GENERAL.ENTER")}
                        buttonVariant={"outlined"}
                        buttonOnPress={() => this.enterAsGussetClick()}
                        points={[
                            {mode: CardPoints.PLUS, text: t("ENTRY_PAGE.PLUS_START_RACE")},
                            {mode: CardPoints.MINUS, text: t("ENTRY_PAGE.MINUS_ADD_HORSE")}
                        ]}
                    />
                </Grid>

                <Grid item xs={8} md={3} className={classes.mar}>
                    <CardPoints
                        title={t("GENERAL.ACCOUNT")}
                        subTitle={t("ENTRY_PAGE.ENTER_AS_ACCOUNT")}
                        buttonTitle={t("GENERAL.SIGN_IN")}
                        buttonOnPress={() => this.signInClick()}
                        points={[
                            {mode: CardPoints.PLUS, text: t("ENTRY_PAGE.PLUS_START_RACE")},
                            {mode: CardPoints.PLUS, text: t("ENTRY_PAGE.PLUS_ADD_HORSE")}
                        ]}
                    />
                </Grid>

            </Grid>
        );
    }

    signInClick() {
        this.props.history.push("/" + PageKeys.LOGIN);
    }

    enterAsGussetClick() {
        this.props.history.push("/" + PageKeys.MAIN);
    }

}

function mapStateToProps(state) {
    return {
        refreshKey: state.data.refreshKey,
    }
}

function mapDispatchToProps(dispatch) {
    return {
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(EntryPage))));