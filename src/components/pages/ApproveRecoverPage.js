import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Grid from "@material-ui/core/es/Grid/Grid";
import Paper from "@material-ui/core/es/Paper/Paper";
import ProcessKeys from "../../keys/ProcessKeys";
import {approveRecoverAsyncAction} from "../../redux/actions/AsyncAction";
import withRouter from "react-router-dom/es/withRouter";
import connect from "react-redux/es/connect/connect";
import Typography from "@material-ui/core/es/Typography/Typography";
import TextField from "@material-ui/core/es/TextField/TextField";
import Button from "@material-ui/core/es/Button/Button";
import Patterns from "../../model/Patterns";
import ActionStatusKeys from "../../keys/ActionStatusKeys";
import MessageDialog from "../MessageDialog";
import {withI18n} from "react-i18next";
import Redirect from "react-router-dom/es/Redirect";
import PageKeys from "../../keys/PageKeys";

const compStyle = theme => ({
    mar: {
        margin: theme.spacing.unit * 2
    },
    container: {
        padding: theme.spacing.unit * 6
    },
    marTop: {
        marginTop: 100
    }
});

class ApproveRecoverPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            passwordValue1: "",
            passwordValue2: "",
            passwordsNotMatch: "",
            passwordValue1Pattern: "",
            passwordValue2Pattern: "",
            isClicked: false,
            onProcess: false,
            recoverModal: false,
            moveLoginPage: false,
            secretCodeValue: "",
        }
    }

    render() {

        const { classes } = this.props;
        const {t} = this.props;

        // TODO present on screen ?
        const email = this.props.match.params.email;

        const passRegx = new RegExp(Patterns.PASSWORD_PATTERN);
        const pass1Error = !passRegx.test(this.state.passwordValue1);
        const pass2Error = !passRegx.test(this.state.passwordValue2);
        const passWordNotMatch = this.state.passwordValue1 !== this.state.passwordValue2;

        const registerOnErrorMode = this.props.approveRecoverProcess === ActionStatusKeys.ERROR;

        // TODO background always white, even if dark mode
        return (
            <Grid container justify={"center"}>

                <MessageDialog
                    open={this.state.recoverModal}
                    color={registerOnErrorMode ? "error" : "primary"}
                    title={t(registerOnErrorMode ? "MESSAGES.APPROVE_RECOVER_FAIL" : "MESSAGES.APPROVE_RECOVER_SUCCESS")}
                    content={t(
                        registerOnErrorMode ?
                            (this.props.error ? "MESSAGES." + this.props.error.toUpperCase() : "")
                            : "MESSAGES.APPROVE_RECOVER_SUCCESS_MESSAGE")}
                    onClickAction={() => this.onDialogClose()}
                    buttonText={t(registerOnErrorMode ? "GENERAL.BACK" : "GENERAL.CONTINUE")}/>

                {this.state.moveLoginPage ? <Redirect to={"/" + PageKeys.LOGIN}/> : null}

                <Grid item>
                    <Paper elevation={10} className={`${classes.container} ${classes.marTop}`}
                           style={{backgroundColor: "#ffffff99"}}>
                        <Grid container justify={"space-around"} direction={"column"} alignItems={"center"}>

                            <Grid item>
                                <Typography
                                    className={classes.mar}
                                    variant={"h3"}
                                    color={this.state.registerOn ? "secondary" : "primary"}>
                                    {t("TITLE.APPROVE_RECOVER_PASSWORD")}
                                </Typography>
                            </Grid>

                            <Grid item>
                                <TextField
                                    error={pass1Error && this.state.isClicked}
                                    id="password1-input"
                                    label={t("GENERAL.NEW_PASSWORD")}
                                    type="password"
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.passwordValue1}
                                    onChange={(event) => this.setState({passwordValue1: event.target.value})}
                                />
                            </Grid>

                            <Grid item>
                                <TextField
                                    error={pass2Error && this.state.isClicked}
                                    id="password2-input"
                                    label={t("GENERAL.PASSWORD_REPEAT")}
                                    type="password"
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.passwordValue2}
                                    onChange={(event) => this.setState({passwordValue2: event.target.value})}
                                />
                            </Grid>

                            <Grid item>
                                <TextField
                                    id="password-input"
                                    label={t("GENERAL.SECRET_CODE")}
                                    type="text"
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.secretCodeValue}
                                    onChange={(event) => this.setState({secretCodeValue: event.target.value})}
                                />
                            </Grid>

                            <Grid item className={classes.mar}>
                                <Button
                                    disabled={this.state.onProcess}
                                    variant={"contained"}
                                    color="primary"
                                    onClick={() => this.onDoneClick()}>
                                    {t("GENERAL.APPROVE_RECOVER")}
                                </Button>
                            </Grid>

                            <Grid item className={classes.mar} styl="">
                                {(pass1Error || pass2Error) && this.state.isClicked ? (
                                    <Typography color={"error"}>
                                        {t("MESSAGES.PASSWORD_ERROR")}
                                    </Typography>
                                ) : null}

                                {passWordNotMatch && this.state.isClicked ? (
                                    <Typography color={"error"}>
                                        {t("MESSAGES.PASSWORDS_NOT_MATCH")}
                                    </Typography>
                                ) : null}
                            </Grid>

                        </Grid>
                    </Paper>

                </Grid>
            </Grid>
        );
    }

    onDoneClick() {
        if(!this.state.isClicked) {
            this.setState({isClicked: true});
        }
        const passRegx = new RegExp(Patterns.PASSWORD_PATTERN);
        if (this.state.passwordValue1 !== this.state.passwordValue2 || !passRegx.test(this.state.passwordValue1) || !passRegx.test(this.state.passwordValue2)) {
            return;
        }
        this.props.approveRecoverAction(this.props.match.params.email, this.state.secretCodeValue, this.state.passwordValue1);
    }

    componentWillReceiveProps(nextProps) {
        const approveRecoverProcess = nextProps.approveRecoverProcess;

        const onProcess = approveRecoverProcess === ActionStatusKeys.START;

        const approveRecoverChanged = approveRecoverProcess !== this.props.approveRecoverProcess;

        const recoverModal = approveRecoverChanged && approveRecoverProcess !== ActionStatusKeys.START && this.props.approveRecoverProcess === ActionStatusKeys.START;

        // TODO find if set set auto check if state props changed
        if (this.state.onProcess !== onProcess ||
            this.state.recoverModal !== recoverModal) {
            this.setState({
                onProcess: onProcess,
                recoverModal: recoverModal,
            });
        }
    }

    onDialogClose() {
        let newState = {recoverModal: false};
        if (this.props.approveRecoverProcess === ActionStatusKeys.SUCCESS) {
            newState = {...newState, moveLoginPage: true};
        }
        this.setState(newState);
    }
}

function mapStateToProps(state) {
    return {
        approveRecoverProcess: state.processes[ProcessKeys.APPROVE_RECOVER_PASSWORD],
        error: state.data.error,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        approveRecoverAction: (email, secretCode, password) => dispatch(approveRecoverAsyncAction(email, secretCode, password)),
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(ApproveRecoverPage))));