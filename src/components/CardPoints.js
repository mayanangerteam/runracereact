import React from "react";
import Paper from "@material-ui/core/es/Paper/Paper";
import withStyles from "@material-ui/core/es/styles/withStyles";
import Typography from "@material-ui/core/es/Typography/Typography";
import PropTypes from 'prop-types';
import withWidth, {isWidthDown} from "@material-ui/core/es/withWidth/withWidth";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove"
import Grid from "@material-ui/core/es/Grid/Grid";
import {Trans, withI18n} from "react-i18next";
import Button from "@material-ui/core/es/Button/Button";

const compStyle = theme => ({
    titleContainer: {
        flex: 1,
        justify: "center"
    },
    card: {
        backgroundColor: theme.palette.grey[200]
    },
    colorGreen: {
        color: "#006400",
    },
    pad: {
        padding: theme.spacing.unit * 4,
    },
    padHalfTop: {
        paddingTop: theme.spacing.unit * 2,
    },
    padHalfBottom: {
        paddingBottom: theme.spacing.unit * 2,
    },
    whiteBackground: {
        backgroundColor: "white"
    },
    buttonContainer: {
        padding: theme.spacing.unit * 2,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center"
    }
});

class CardPoints extends React.Component {

    static MINUS = "minus";
    static PLUS = "pluse";

    render() {

        const {classes} = this.props;

        const variantSize = isWidthDown("xs", this.props.width, true) ? 5 : 4;
        const variantTitle = "h" + variantSize;
        const variantSubTitle = "subtitle1"; // "display" + (variantSize + 1);

        return (
            <Paper className={`${classes.card}`} elevation={10} style={{height: "100%"}}>

                <div className={classes.titleContainer}>
                    <div className={`${classes.pad} ${classes.padHalfBottom}`}>
                        <Typography variant={variantTitle} align="center">
                            {this.props.title}
                        </Typography>
                        <Typography variant={variantSubTitle}>
                            {this.props.subTitle}
                        </Typography>
                    </div>
                </div>

                <div className={`${classes.pad} ${classes.whiteBackground} ${classes.padHalfTop}`}>
                    {this.props.points.map((point, i) => (
                        <Grid container className={classes.colorGreen} key={i}>
                            <Grid item xs={3} md={2}>
                                {point.mode === CardPoints.MINUS ? <RemoveIcon color={"error"}/> : <AddIcon/>}
                            </Grid>
                            <Grid item xs>
                                <Typography variant={"body1"}
                                            color={point.mode === CardPoints.MINUS ? "error" : "inherit"}>
                                    {point.text}
                                </Typography>
                            </Grid>
                        </Grid>
                    ))}

                    <div className={`${classes.buttonContainer}`}>
                        <Button
                            onClick={() => this.props.buttonOnPress && this.props.buttonOnPress()}
                            variant={this.props.buttonVariant ? this.props.buttonVariant : "contained"}
                            color="primary">
                            {this.props.buttonTitle}
                        </Button>
                    </div>

                </div>

            </Paper>
        )
    }

}

CardPoints.propTypes = {
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
    points: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string.isRequired,
        mode: PropTypes.oneOf([CardPoints.MINUS, CardPoints.PLUS])
    })).isRequired,
    buttonTitle: PropTypes.string,
    buttonVariant: PropTypes.string,
    buttonOnPress: PropTypes.func
};

export default withWidth()(withStyles(compStyle)(CardPoints));