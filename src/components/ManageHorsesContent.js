import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Grid from "@material-ui/core/es/Grid/Grid";
import Paper from "@material-ui/core/es/Paper/Paper";
import {withI18n} from "react-i18next";
import withRouter from "react-router-dom/es/withRouter";
import connect from "react-redux/es/connect/connect";
import Table from "@material-ui/core/es/Table/Table";
import TableHead from "@material-ui/core/es/TableHead/TableHead";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import TableBody from "@material-ui/core/es/TableBody/TableBody";
import Button from "@material-ui/core/es/Button/Button";
import Typography from "@material-ui/core/es/Typography/Typography";
import {getHorsesAsyncAction} from "../redux/actions/AsyncAction";

const compStyle = theme => ({

});

class ManageHorsesContent extends React.Component {

    render() {

        const { classes, t } = this.props;

        return (
            <div style={{textAlign: "center", width: "100%"}}>

                {this.props.horses ? (
                    <div style={{textAlign: "center", width: "100%"}}>
                        <Typography variant={"h4"} color="primary">{t("CONTENT.YOUR_HORSES")}</Typography>

                        <Table className={classes.table}>
                            <TableHead>
                                <TableRow>
                                    <TableCell>{t("HORSE.NICKNAME")}</TableCell>
                                    <TableCell>{t("HORSE.PRIVATE_NAME")}</TableCell>
                                    <TableCell>{t("HORSE.RACE")}</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.props.horses.map(horse => {
                                    return (
                                        <TableRow key={horse.id}>
                                            <TableCell>{horse.nickname}</TableCell>
                                            <TableCell>{horse.privateName}</TableCell>
                                            <TableCell>{horse.race}</TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </div>
                ) : (
                    <Typography variant={"h4"} color="primary">{t("CONTENT.LOADING")}</Typography>
                )}

            </div>
        );
    }

    componentDidMount() {
        this.props.getHorsesAction();
    }

}

function mapStateToProps(state) {
    return {
        horses: state.horses.horses,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getHorsesAction: () => dispatch(getHorsesAsyncAction())
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(ManageHorsesContent))));