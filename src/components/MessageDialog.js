import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Dialog from "@material-ui/core/es/Dialog/Dialog";
import DialogTitle from "@material-ui/core/es/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/es/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/es/DialogContentText/DialogContentText";
import DialogActions from "@material-ui/core/es/DialogActions/DialogActions";
import Button from "@material-ui/core/es/Button/Button";
import Typography from "@material-ui/core/es/Typography/Typography";
import PropTypes from "prop-types";

const compStyle = theme => ({

});

class MessageDialog extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { classes } = this.props;

        const color = this.props.color ? this.props.color : "primary";

        return (
            <Dialog
                open={this.props.open}
                onClose={() => this.props.onClose && this.props.onClose()}
                aria-labelledby="scroll-dialog-title">
                <DialogTitle id="scroll-dialog-title">
                    <Typography color={color}>{this.props.title}</Typography>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {this.props.content}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.onClickAction && this.props.onClickAction()} color="primary">
                        {this.props.buttonText}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

MessageDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string,
    buttonText: PropTypes.string.isRequired,
    color: PropTypes.string,
    onClose: PropTypes.func,
    onClickAction: PropTypes.func,
};

export default withStyles(compStyle)(MessageDialog);