import React from 'react';
import withStyles from "@material-ui/core/es/styles/withStyles";
import Grid from "@material-ui/core/es/Grid/Grid";
import Paper from "@material-ui/core/es/Paper/Paper";
import {withI18n} from "react-i18next";
import withRouter from "react-router-dom/es/withRouter";
import connect from "react-redux/es/connect/connect";
import {addHorseAsyncAction} from "../redux/actions/AsyncAction";
import ProcessKeys from "../keys/ProcessKeys";
import ActionStatusKeys from "../keys/ActionStatusKeys";
import TextField from "@material-ui/core/es/TextField/TextField";
import Button from "@material-ui/core/es/Button/Button";
import Typography from "@material-ui/core/es/Typography/Typography";
import Patterns from "../model/Patterns";

const compStyle = theme => ({

});

class AddHorseContent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            onProcess: false,
            showMessage: true,
            showSyntax: false,

            nicknameValue: "",
            privateNameValue: "",
            raceValue: "",
        };
    }

    render() {

        const { classes, t } = this.props;

        return (
            <Grid container justify={"center"} alignItems={"center"} direction={"column"}>

                <Grid item>
                    <TextField
                        id="nickname-input"
                        label={this.props.t("HORSE.NICKNAME")}
                        type="text"
                        margin="normal"
                        variant="outlined"
                        value={this.state.nicknameValue}
                        onChange={(event) => this.setState({nicknameValue: event.target.value})}
                    />
                </Grid>

                <Grid item>
                    <TextField
                        id="private-name-input"
                        label={this.props.t("HORSE.PRIVATE_NAME")}
                        type="text"
                        margin="normal"
                        variant="outlined"
                        value={this.state.privateNameValue}
                        onChange={(event) => this.setState({privateNameValue: event.target.value})}
                    />
                </Grid>

                <Grid item>
                    <TextField
                        id="race-input"
                        label={this.props.t("HORSE.RACE")}
                        type="text"
                        margin="normal"
                        variant="outlined"
                        value={this.state.raceValue}
                        onChange={(event) => this.setState({raceValue: event.target.value})}
                    />
                </Grid>

                <Grid item>
                    <Button variant={"contained"} color={"primary"} disabled={this.state.onProcess}
                            onClick={() => this.onAddClick()}>
                        {t("GENERAL.ADD")}
                    </Button>
                </Grid>

                <Grid item>
                    {this.state.showSyntax ? (
                        <Typography color="error">{t("MESSAGES.HORSE_SYNTAX")}</Typography>
                    ) : null}
                    {this.props.addHorseProcess === ActionStatusKeys.SUCCESS && this.state.showMessage ? (
                        <Typography color="primary">{t("MESSAGES.ADD_HORSE_SUCCESS")}</Typography>
                    ) : null}
                    {this.props.addHorseProcess === ActionStatusKeys.ERROR && this.state.showMessage ? (
                        <Typography color="error">{t("MESSAGES.ABSTRACT_ERROR")}</Typography>
                    ) : null}
                </Grid>


            </Grid>
        );

    }

    componentWillReceiveProps(nextProps) {
        const addProcess = nextProps.addHorseProcess;
        const addProcessChanged = addProcess !== this.props.addHorseProcess;

        // TODO maybe to redux state onProcess

        // TODO find if setState set auto check if state props changed
        if (addProcessChanged !== ActionStatusKeys.START && this.state.onProcess) {
            this.setState({
                onProcess: false,
                showMessage: true,
            });
        }
    }

    onAddClick() {
        const simpleRegx = new RegExp(Patterns.REGULAR_PATTERN);
        if ( ! (this.state.nicknameValue &&
                this.state.privateNameValue &&
                simpleRegx.test(this.state.nicknameValue) &&
                simpleRegx.test(this.state.privateNameValue) &&
                ( !this.state.raceValue || simpleRegx.test(this.state.raceValue) ) )) {
            this.setState({showSyntax: true});
            return;
        }

        this.setState({showSyntax: false, onProcess: true});
        this.props.addHorseAction({nickname: this.state.nicknameValue, privateName: this.state.privateNameValue, race: this.state.raceValue});
    }
}

function mapStateToProps(state) {
    return {
        addHorseProcess: state.processes[ProcessKeys.ADD_HORSE],
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addHorseAction: (horse) => dispatch(addHorseAsyncAction(horse))
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withI18n()(withStyles(compStyle)(AddHorseContent))));